package net.whitneyhunter.spring.beanpostprocessor.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.UUID;

import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.whitneyhunter.spring.beanpostprocessor.annotation.TenantInjected;
import net.whitneyhunter.spring.beanpostprocessor.state.TenantIdThreadLocal;

/**
 * A Spring {@link BeanPostProcessor} that will inject that tenant id from the {@link TenantIdThreadLocal} into
 * beans that have a this method: {@code public void setTenantId(UUID tenantId)}.
 */
@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class TenantIdInjectingPostProcessor implements BeanPostProcessor {

    /**
     * A {@link BeanPostProcessor} that will check for fields that should have the thread local tenant id injected into
     * them. When found, do the injection.
     *
     * @param bean The bean to check.
     * @param beanName The name of of the bean to check.
     * @return The resulting bean.
     * @throws BeansException Unable to post process bean.
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        UUID tenantId = TenantIdThreadLocal.INSTANCE.getTenantId();
        Class<?> targetClass = AopProxyUtils.ultimateTargetClass(bean);
        ReflectionUtils.doWithMethods(targetClass,
                methodToProcess -> setTenantId(bean, methodToProcess, tenantId),
                this::isMethodInjectable);

        return bean;
    }

    /**
     * Use refletion call the given {@link Method} to set the tenant id.
     *
     * @param bean The bean on which the tenant id should be set.
     * @param method The {@link Method} that should be used to set the tenant id
     * @param tenantId The {@link UUID} that represents the tenant id.
     * @throws IllegalAccessException
     */
    private void setTenantId(Object bean, Method method, UUID tenantId) throws IllegalAccessException {
        try {
            method.invoke(bean, tenantId);
        } catch (InvocationTargetException e) {
            throw new IllegalAccessException("Unable to set tenant id");
        }
    }

    /**
     * In order to inject a tenant, the method must be annotated with @TanantInjected and take a UUID. This
     * method checks these conditions.
     *
     * @param method The {@link Method} to check.
     * @return True if a tenant ID should be injected into the field.
     */
    private boolean isMethodInjectable(Method method) {
        boolean isAnnotated = Objects.nonNull(method.getAnnotation(TenantInjected.class));
        boolean takesUUID = method.getParameterCount() == 1 && UUID.class.equals(method.getParameterTypes()[0]);
        return isAnnotated && takesUUID;
    }

}
