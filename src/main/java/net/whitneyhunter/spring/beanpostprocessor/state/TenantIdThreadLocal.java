package net.whitneyhunter.spring.beanpostprocessor.state;

import java.util.UUID;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Java thread local singleton that holds a {@link UUID} that is the tenant id.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TenantIdThreadLocal {

    private ThreadLocal<UUID> valueContainer = new ThreadLocal<>();

    public static final TenantIdThreadLocal INSTANCE = new TenantIdThreadLocal();

    /**
     * Put the tenant id into the thread local.
     *
     * @param tenantId The new tenant id to store.
     */
    public void setTenantId(UUID tenantId) {
        valueContainer.set(tenantId);
    }

    /**
     * Get the tenant id from the thread local.
     *
     * @return The tenant id from the thread local.
     */
    public UUID getTenantId() {
        return valueContainer.get();
    }

}
