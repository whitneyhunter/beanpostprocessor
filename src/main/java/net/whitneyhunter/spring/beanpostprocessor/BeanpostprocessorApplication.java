package net.whitneyhunter.spring.beanpostprocessor;

import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.whitneyhunter.spring.beanpostprocessor.state.TenantIdThreadLocal;

@SpringBootApplication
public class BeanpostprocessorApplication {

    public static void main(String[] args) {
        TenantIdThreadLocal.INSTANCE.setTenantId(UUID.randomUUID());
        SpringApplication.run(BeanpostprocessorApplication.class, args);
    }

}

