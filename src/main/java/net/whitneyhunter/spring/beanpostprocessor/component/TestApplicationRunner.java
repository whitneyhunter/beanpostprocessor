package net.whitneyhunter.spring.beanpostprocessor.component;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Component that will create a new {@link TenantIdRequiringComponent} and call the {@link TenantIdRequiringComponent#doSomething()}
 * method on it.
 */
@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class TestApplicationRunner implements ApplicationRunner {

    private final ObjectFactory<TenantIdRequiringComponent> componentFactory;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        TenantIdRequiringComponent tenantIdRequiringComponent = componentFactory.getObject();
        tenantIdRequiringComponent.doSomething();
    }

}
