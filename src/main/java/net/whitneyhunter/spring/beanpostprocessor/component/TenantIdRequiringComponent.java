package net.whitneyhunter.spring.beanpostprocessor.component;

import java.util.UUID;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import net.whitneyhunter.spring.beanpostprocessor.annotation.TenantInjected;

/**
 * Component that should have the tenant id injected into it.
 */
@Component
@Scope("prototype")
@Slf4j
public class TenantIdRequiringComponent {

    private UUID tenantId;

    @TenantInjected
    public void setTenantId(UUID tenantId) {
        this.tenantId = tenantId;
    }

    public void doSomething() {
        log.info("The tenant UUID value is {}", tenantId);
    }

}
